from django.shortcuts import render, redirect
import random
from .models import MataKuliah
from .forms import ProductForm

# Create your views here.
def home(request):
    return render(request, 'home.html')
def cv(request):
    return render(request, 'cv.html')
def aboutme(request):
    return render(request, 'about_me.html')
def demo(request):
    isEvenNumberFound = False
    while (not isEvenNumberFound):
        random_number = random.randint(0, 101)
        if (random_number % 2 == 0):
            isEvenNumberFound = True
    
    context = {
        'random_number_value' : random_number,
    }

    return render(request, 'number.html', context)

def form(request):
    form = ProductForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('display')

    context = {
       'form' : form
    }
    return render(request, 'forms.html', context)

def display(request):
    obj  = MataKuliah.objects.all()
    context = {
        'object' : obj
    }
    return render(request, 'data_display.html', context)

def detail(request, my_id):
    obj = MataKuliah.objects.get(id=my_id)
    context = {
        'object':obj
    }
    return render(request, "details.html", context)

def delete_view(request, my_id): 
    obj = MataKuliah.objects.get(id=my_id)
    obj.delete()  
    return redirect ('display') 

