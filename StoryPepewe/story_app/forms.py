from django import forms
from .models import MataKuliah

class ProductForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'mata_kuliah',
            'dosen',
            'jumlah_sks',
            'semester',
            'kelas',
            'description'
        ]
    
