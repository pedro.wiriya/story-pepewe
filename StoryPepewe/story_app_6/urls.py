from django.urls import path
from .views import display_activities, register_activities, register_people

urlpatterns = [
    path('activities/', display_activities, name='activities-display'),
    path('activities/register-people/', register_people, name='register-people'),
    path('activities/register-activities/', register_activities, name='register-activities')
]
