from django.apps import AppConfig


class StoryApp6Config(AppConfig):
    name = 'story_app_6'
